//
//  GyroscopeViewController.swift
//  swift_Dynamics
//
//  Created by 李彦宏 on 2019/8/1.
//  Copyright © 2019 LYH. All rights reserved.
//

import UIKit
import PKHUD
class GyroscopeViewController: YHBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     self.title = "陀螺仪"
        //右边按钮
     self.initNavaRightButton()
    HUD.dimsBackground = false
    }
    
    func initNavaRightButton() -> Void {
    
        let rightButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 80, height: 44))
            rightButton.setTitle("陀螺仪介绍", for: .normal)
            rightButton.setTitleColor(.white, for: .normal)
        rightButton.setTitleColor(UIColor.gray, for: .highlighted)
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        rightButton.addTarget(self, action: #selector(rightButtonClick), for: .touchUpInside)
      self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: rightButton)
    }
    
    
    @objc func rightButtonClick() -> Void {
   
     HUD.flash(.label("规则就是...你猜啊"), delay: 1.3)
    }

    //释放
    deinit {
        HUD.deregisterFromKeyboardNotifications()
    }
}
