//
//  CoreMotionViewController.swift
//  swift_Dynamics
//
//  Created by 李彦宏 on 2019/7/31.
//  Copyright © 2019 LYH. All rights reserved.
//
/**
 *1.陀螺仪
 *2.加速计
 */

import UIKit

class CoreMotionViewController: YHBaseViewController , UITableViewDelegate , UITableViewDataSource{
  
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableview)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellid")
        if cell == nil {
           cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cellid")
        }
        cell?.textLabel?.text = ["陀螺仪","加速计"][indexPath.row]
        
        return cell!
    }
    //点击事件
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
    self.navigationController?.pushViewController(GyroscopeViewController(), animated: true)
        }
    }
    
    lazy var tableview: UITableView = {
        let tableview = UITableView.init(frame: CGRect.init(x: 0, y: 0, width: KscreenWidth, height: KscreenHeight-SafeAreaTopHeight-SafeAreaTabBarHeight), style: UITableView.Style.grouped)
        if #available(iOS 11.0, *) {
            tableview.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        tableview.rowHeight = 50
        tableview.backgroundColor = UIColor.white
        tableview.estimatedRowHeight = 0
        tableview.estimatedSectionFooterHeight = 0
        tableview.estimatedSectionHeaderHeight = 0
        //分割线撑满
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0 )
        //去除多余分割线
        let view = UIView()
        tableview.tableFooterView = view
        tableview.tableHeaderView = view
        tableview.delegate = self
        tableview.dataSource = self
       
        return tableview
    }()
}
