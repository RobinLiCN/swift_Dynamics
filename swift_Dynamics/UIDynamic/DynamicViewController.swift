//
//  DynamicViewController.swift
//  swift_Dynamics
//
//  Created by 李彦宏 on 2019/7/31.
//  Copyright © 2019 LYH. All rights reserved.
//

import UIKit

class DynamicViewController: YHBaseViewController ,UITableViewDelegate,UITableViewDataSource{
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableview)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellid")
        if cell == nil  {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "cellid")
        }
        cell?.textLabel?.text = dataArray[indexPath.row] as? String
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    lazy var tableview: UITableView = {
        let tableview = UITableView.init(frame: .init(x: 0, y: 0, width: KscreenWidth, height: KscreenHeight-SafeAreaTopHeight-SafeAreaTabBarHeight), style: .plain)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.rowHeight = 50
        tableview.backgroundColor = UIColor.white
        tableview.tableFooterView = UIView()
        //分割线
        tableview.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        return tableview
    }()
    lazy var dataArray: NSArray = {
        let dataArray = NSArray.init(array: ["重力行为","碰撞行为","捕捉行为","推动行为","附着行为","动力元素行为"])
        return dataArray
    }()
    
}
