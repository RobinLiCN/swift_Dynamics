//
//  YHBaseViewController.swift
//  YHSwiftProject
//
//  Created by LYH on 2018/11/5.
//  Copyright © 2018年 LYH. All rights reserved.
//

import UIKit

class YHBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    view.backgroundColor = UIColor.white
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.view.endEditing(true)
    }
}
