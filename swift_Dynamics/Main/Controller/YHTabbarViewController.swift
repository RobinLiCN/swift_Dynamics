//
//  YHTabbarViewController.swift
//  YHSwiftProject
//
//  Created by LYH on 2018/11/2.
//  Copyright © 2018年 LYH. All rights reserved.
//
import Foundation
import UIKit

class YHTabbarViewController: UITabBarController{
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // 背景色设为白色
        self.view.backgroundColor = UIColor.white
          /**设置字体大小颜色*/
        let tabBar = UITabBarItem.appearance()
        
        let attrsNormal = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0), NSAttributedString.Key.foregroundColor: HexColor(hexstring: "515151")]
        let attrsSelected = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0), NSAttributedString.Key.foregroundColor: HexColor(hexstring: "d4237a")]
        tabBar.setTitleTextAttributes(attrsNormal, for: .normal)
        tabBar.setTitleTextAttributes(attrsSelected, for: .selected)
       
        
       setUpAllviewcontroller()
    }
  // MARK: ---------- 添加子控件 ----------
    func setUpAllviewcontroller() -> Void {
    setChildViewController(childController: CoreMotionViewController(), title: "CoreMotion", norImage: "firstNormal", selectedImage: "firstSelect",itemTag: 0)
         setChildViewController(childController: DynamicViewController(), title: "Dynamic", norImage: "secendSel", selectedImage: "secendNormal",itemTag: 1)
    }
    
// MARK: --------- 初始化子控制器----------
    private func setChildViewController(childController: UIViewController,title:String,norImage:String,selectedImage:String,itemTag:NSInteger){
        childController.tabBarItem.tag = itemTag;
      childController.tabBarItem.title = title
      //设置Tabbar 标题位置
//      childController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3)
    childController.tabBarItem.image = UIImage(named: norImage)
   //去除图片渲染
    childController.tabBarItem.selectedImage = UIImage(named: selectedImage)?.withRenderingMode(.alwaysOriginal)
    //设置 NavigationController
    let nav = YHNavigationController.init(rootViewController: childController)
        //标题颜色
        nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        //背景颜色
//        nav.navigationBar.barTintColor = HexColor(hexstring: "d4237a")
        
        nav.navigationBar.setBackgroundImage(UIImage.init(named: "navaStar"), for: UIBarMetrics.default)
        self.addChild(nav)
        childController.title = title
    }
    // MARK: - /********************** UITabBarControllerDelegate **********************/
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {

//        print("item=%d",item.tag)
        //点击tabbarItem 动画
     self.tabBarButtonClick(tabBarButton: self.getTabBarButton(selectindex: item.tag))
    }
    
    func getTabBarButton(selectindex:NSInteger) -> UIControl {
        let tabBarButtons = NSMutableArray.init()
        for tabBarButton in self.tabBar.subviews {
            if NSStringFromClass(tabBarButton.classForCoder) == "UITabBarButton"
            {
               
                tabBarButtons.add(tabBarButton)
            }
        }
        let tabBarButton:UIControl = tabBarButtons.object(at:selectindex) as! UIControl
        
        return tabBarButton
    }
    
    
   //点击动画
    func tabBarButtonClick(tabBarButton:UIControl) -> Void {
        for imageview in tabBarButton.subviews {
           
            if NSStringFromClass(imageview.classForCoder) == "UITabBarSwappableImageView"
            {
                let animation = CAKeyframeAnimation()
                animation.keyPath = "transform.scale"
                animation.values = [1.0,1.25,0.8,1.25,1.0]
                animation.duration = 0.5
                animation.calculationMode = CAAnimationCalculationMode.cubic
                imageview.layer.add(animation, forKey: "")
                
            }
        }
    }
    
    
}
